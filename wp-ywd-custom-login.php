<?php
/*
Plugin Name: Custom WP login
Description: Add possibility to customize wp login panel
Author: Yes We Dev
Version: 1.0
Author URI: https://yeswedev.bzh
*/

add_action('admin_menu', 'my_plugin_menu');
add_action( 'admin_init', 'custom_login_init' );
function my_plugin_menu() {
    add_menu_page('Custom WP login', 'Custom WP login', 'administrator', 'custom_login_page', 'custom_login_page', 'dashicons-admin-network');
}

function custom_login_init(){
    register_setting('custom_login','custom_login_settings');
    add_settings_section(
        'custom_login_settings_section',
        '',
        '',
        'custom_login_admin_page'
    );

    add_settings_field('image_url','URL de l\'image','image_url_render','custom_login_admin_page','custom_login_settings_section' );
    add_settings_field('image_name','Titre au survol','image_name_render','custom_login_admin_page','custom_login_settings_section' );

}

function image_url_render() {
    $options = get_option( 'custom_login_settings' );
    ?>
    <input id="custom_login_image_url" type='text' name='custom_login_settings[image_url]'
           value='<?php echo isset($options['image_url']) ? esc_attr($options['image_url']) : ""; ?>'>
    <?php
}

function image_name_render() {
    $options = get_option( 'custom_login_settings' );
    ?>
    <input id="custom_login_image_name" type='text' name='custom_login_settings[image_name]'
           value='<?php echo isset($options['image_name']) ? esc_attr($options['image_name']) : ""; ?>'>
    <?php
}

function custom_login_page() {
    $options = get_option( 'custom_login_settings' );
    $image_url = esc_attr( $options['image_url'] );
    $image_name = esc_attr( $options['image_name'] );
    if ( isset( $_REQUEST['settings-updated'] ) ) {
        ?>
        <div class="updated"><p><strong><?php echo 'Réglages enregistrés'; ?></strong></p></div><?php
    }
    ?>

    <div class="wrap">
        <h2>Personnalisation du panneau d'identification</h2>
        <form method="post" action="options.php">
            <h3>Image selectionnée</h3>
            <img src="<?php echo $image_url;?>" id="image_preview" alt="">
            <input type="button" name="upload-btn" id="upload-btn" class="button-secondary" value="Upload Image">
            <?php
            settings_fields( 'custom_login' );
            do_settings_sections( 'custom_login_admin_page' );
            ?>
            <?php submit_button(); ?>
        </form>
    </div>
    <?php
}


function wp_login_image() {
    register_setting( 'custom_login_image', 'image_url',array(
        'type' => 'string',
        'default' => site_url().'/wp-admin/images/wordpress-logo.svg',
    ));
    register_setting( 'custom_login_image', 'image_name',array(
        'type' => 'string',
        'default' => '',
    ));
}
add_action( 'admin_init', 'wp_login_image' );


function addMediaUploadScript($hook){
    if($hook != 'toplevel_page_custom_login_page') {
        return;
    }
    wp_enqueue_style( 'custom_wp_login_admin_css', plugins_url('/css/admin-style.css', __FILE__) );
    wp_enqueue_script('jquery');
    wp_enqueue_script('admin_media', plugins_url('/js/admin_media.js',__FILE__));
    wp_enqueue_media();
}
add_action('admin_enqueue_scripts','addMediaUploadScript');

function custom_login_image() {
    $options = get_option('custom_login_settings');
    if (!$options) {
        return;
    } else {
        if (!isset($options['image_url'])) {
            return;
        }
        ?>
        <style type="text/css">
            #login h1 a,
            .login h1 a {
                background: url("<?php echo esc_attr( $options['image_url'] ) ?>") no-repeat center center;
                background-size: auto;
                width: 100%;
            }
        </style>
        <?php
    }
}

function my_login_logo_url() {
    return home_url();
}
function my_login_logo_url_title() {
    $options = get_option( 'custom_login_settings' );
    if(!$options) {
        return 'Propulsé par Wordpress';
    } else {
        if(!isset($options['image_name'])){
            return 'Propulsé par Wordpress';
        }
        $image_name = esc_attr( $options['image_name'] );
        return $image_name;
    }
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );

add_filter( 'login_headerurl', 'my_login_logo_url' );

add_action( 'login_enqueue_scripts', 'custom_login_image' );

/*
Copyright 2018 Yes We Dev (http://yeswedev.bzh)
*/

